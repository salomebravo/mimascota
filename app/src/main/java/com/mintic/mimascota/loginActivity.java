package com.mintic.mimascota;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class loginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnAcceder = findViewById(R.id.login_btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Establecemos los Valores de las cajas de texto
                EditText email =findViewById(R.id.login_email);
                EditText clave = findViewById(R.id.login_clave);

                //Obtenemos los valores de las cajas de texto
                String valorEmail = email.getText().toString();
                String valorClave = clave.getText().toString();

                //Verificamos los valores
                if(!valorEmail.isEmpty() && !valorClave.isEmpty()){
                    Intent intent =new Intent(v.getContext(), MainUserMenuActivity.class);
                    intent.putExtra("email", valorEmail);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(v.getContext(), "Digite todos los campos requeridos.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}